import User from './src/stores/User';
import Home from './src/stores/Home';

const user = new User();
const home = new Home();

export default {

    user: user,
    home: home
}