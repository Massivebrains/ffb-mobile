import React from 'react';
import { StyleSheet, Image, View } from 'react-native';
import { Container, Button, Content, Text } from 'native-base';
import Head from './Head';
import Colors from './Colors';
import { inject, observer } from 'mobx-react';
const Local = require('./Local');

@inject('user')
@observer
export default class Auth extends React.Component {

    constructor(props) {

        super(props);
        this.state = { loading: true }
    }

    register = () => {

        this.props.navigation.navigate('Register');
    }

    login = () => {

        this.props.navigation.navigate('Login');
    }

    async componentDidMount() {

        this.didFocusListener = this.props.navigation.addListener('didFocus', this.load);
        this.load();
    }

    load = async () => {

        let user = await Local.get('user');

        if (user) {

            this.navigation.navigate('Chat');
        }
    }

    render() {

        return (

            <Container style={styles.container}>

                <Head title='Authentication' back='Home' navigation={this.props.navigation} />

                <Content contentContainerStyle={{ flex: 1 }}>

                    <Image source={require('./images/authentication.png')} style={{ width: '100%', height: 300, alignSelf: 'center' }} />
                    <Text style={{ color: Colors.primary, fontSize: 20, fontWeight: 'normal', marginTop: 20, marginBottom: 30, textAlign: 'center' }}>Join FFB to gain access to your Fans Chat rooms!</Text>

                    <Text style={{ color: Colors.primary, fontWeight: 'normal', textAlign: 'center', marginBottom: 5 }}>Don't Have an acount?</Text>

                    <Button onPress={this.register} rounded block style={{ alignSelf: 'center', elevation: 0, marginLeft: 20, marginRight: 20, background: Colors.primary }}><Text>Register</Text></Button>

                    <View style={{ borderBottomWidth: StyleSheet.hairlineWidth, marginTop: 20 }}></View>

                    <Text style={{ color: Colors.primary, fontWeight: 'normal', textAlign: 'center', marginBottom: 5, marginTop: 30 }}>Or already have an account ? </Text>
                    <Button onPress={this.login} rounded block style={{ alignSelf: 'center', elevation: 0, marginLeft: 20, marginRight: 20 }}><Text>Login Now</Text></Button>

                </Content>

            </Container>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-end'
    },
    indicator: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        height: '100%'
    }

});