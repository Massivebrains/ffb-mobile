import React from 'react';
import { Header, Body, Title, Left, Icon, Right, Thumbnail, Button } from 'native-base';
const colors = require('./Colors');
import { inject, observer } from 'mobx-react';

@inject('user')
@observer
export default class Head extends React.Component {

    constructor(props) {

        super(props);

        this.state = {

            thumbnail: 'https://res.cloudinary.com/dlk7iozek/image/upload/v1550663534/FFB/1WGjauXxEUtz0wyD26GRUOF8plIrSjrH.png'
        }
    }

    componentDidMount(){

        if(this.props.user.photo)
            this.setState({thumbnail : this.props.user.photo});
    }

    render() {

        if (this.props.back != undefined) {

            return (

                <Header style={{ backgroundColor: colors.primary }} androidStatusBarColor={colors.primary}>
                    <Left>
                        <Icon name='arrow-back' style={{ color: colors.white }} onPress={() => this.props.navigation.navigate(this.props.back)} />
                    </Left>
                    <Body>
                        <Title>{this.props.title}</Title>
                    </Body>
                    <Right>
                        <Button transparent>
                            <Icon name='md-mail' style={{ color: 'white' }} />
                        </Button>
                    </Right>
                </Header>
            )
        }

        return (

            <Header style={{ backgroundColor: colors.primary }} androidStatusBarColor={colors.primary}>
                <Left>
                    <Thumbnail small source={{ uri: this.props.user.photo ? this.props.user.photo : this.state.thumbnail }} />
                </Left>
                <Body>
                    <Title style={{ color: 'white' }}>{this.props.title}</Title>
                </Body>
                <Right>
                    <Button transparent>
                        <Icon name='md-mail' style={{ color: 'white' }} />
                    </Button>
                </Right>
            </Header>
        );
    }
}