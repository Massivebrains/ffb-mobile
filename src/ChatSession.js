import React from 'react';
import { StyleSheet, ActivityIndicator } from 'react-native';
import { Container, Content } from 'native-base';
import { GiftedChat, Bubble } from 'react-native-gifted-chat';
import Head from './Head';
import Colors from './Colors';
import ChatKit from '@pusher/chatkit';
import Loading from './Loading';
const Utils = require('./Utils');
import { inject, observer } from 'mobx-react';


const CHATKIT_TOKEN_PROVIDER_ENDPOINT = 'https://us1.pusherplatform.io/services/chatkit_token_provider/v1/1683996c-bd7d-4484-82d8-88caa0f0ca8d/token';
const CHATKIT_INSTANCE_LOCATOR = 'v1:us1:1683996c-bd7d-4484-82d8-88caa0f0ca8d';

@inject('user')
@observer
export default class ChatSession extends React.Component {

    static navigationOptions = { tabBarVisible: false }

    constructor(props) {

        super(props);
        this.state = { messages: [], loading: true, chat: {}, room_id: 0, username: '' };
    }

    async componentDidMount() {

        this.load();
        this.didFocusListener = this.props.navigation.addListener('didFocus', this.load);
    }

    load = async () => {

        let chat = this.props.navigation.getParam('chat');

        if (!chat || !chat.room_id || !chat.username || chat.username.length < 1) {

            this.props.navigation.navigate('Chats');

        } else {

            this.setState({ chat: chat, room_id: parseInt(chat.room_id), username: chat.username });

            if (this.state.username.length > 0 && this.state.room_id > 0)
                this.initalizeChat();
        }

    }

    initalizeChat = async () => {

        const tokenProvider = new ChatKit.TokenProvider({ url: CHATKIT_TOKEN_PROVIDER_ENDPOINT });
        const chatManager = new ChatKit.ChatManager({ instanceLocator: CHATKIT_INSTANCE_LOCATOR, userId: this.state.username, tokenProvider: tokenProvider });

        chatManager.connect().then(currentUser => {

            this.currentUser = currentUser;
            this.currentUser.subscribeToRoom({ roomId: this.state.room_id, hooks: { onNewMessage: this.onReceive.bind(this) } });

        }).catch(error => {

            Utils.toast(JSON.stringify(error))
        });
    }

    onReceive(data) {

        const { id, senderId, text, createdAt } = data;
        const incomingMessage = {

            _id: id,
            text: text,
            createdAt: new Date(createdAt),
            user: { _id: senderId, name: senderId }
        }

        this.setState(previousState => ({ messages: GiftedChat.append(previousState.messages, incomingMessage) }));
    }

    onSend([message]) {

        this.currentUser.sendMessage({ text: message.text, roomId: this.state.room_id });
    }

    renderBubble(props) {

        return (<Bubble {...props} wrapperStyle={{ right: { backgroundColor: Colors.primary } }} />);
    }

    renderLoading() {

        return (<ActivityIndicator animating={true} size={30} style={styles.indicator} color={Colors.primary} />);
    }

    render() {

        if (this.loading == true) return (<Loading title='Chat' navigation={this.props.navigation} back='Home' />);

        return (

            <Container style={styles.container}>

                <Head title={this.state.chat.name} back='Home' navigation={this.props.navigation} />

                <Content contentContainerStyle={{ flex: 1 }}>

                    {this.state.username.length > 0 && this.state.room_id ? (

                        <GiftedChat messages={this.state.messages} onSend={messages => this.onSend(messages)} user={{ _id: this.state.username }} renderUsernameOnMessage={true} renderBubble={props => this.renderBubble(props)} isAnimated={true} renderLoading={this.renderLoading} />

                    ) : null}

                </Content>

            </Container>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-end'
    },
    indicator: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        height: '100%'
    }

});