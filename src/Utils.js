import { ToastAndroid } from 'react-native';

module.exports = {

    error_message: 'Could not connect at this time.',
    toast: (text = 'Could not connect at this time.') => ToastAndroid.showWithGravity(text, ToastAndroid.LONG, ToastAndroid.BOTTOM)
}