import React from 'react';
import { StyleSheet, View, TouchableOpacity } from 'react-native';
import { Container, Content, H1, Text, Thumbnail, Button, List, ListItem, Left, Right } from 'native-base';
import Head from './Head';
import PhotoUpload from 'react-native-photo-upload';
import { SkypeIndicator } from 'react-native-indicators';
const Colors = require('./Colors');
import { inject, observer } from 'mobx-react';
const Utils = require('./Utils');
const API = require('./API');
const Local = require('./Local');
import { StackActions, NavigationActions } from 'react-navigation';

@inject('user')
@observer
export default class Profile extends React.Component {

    constructor(props) {

        super(props);

        this.state = {

            name: '',
            username: null,
            email: null,
            phone: null,
            thumbnail: 'https://res.cloudinary.com/dlk7iozek/image/upload/v1550663534/FFB/1WGjauXxEUtz0wyD26GRUOF8plIrSjrH.png',
            uploading: false
        };
    }

    async componentDidMount() {

        if (!this.props.user.user)
            this.props.navigation.navigate('Login');

        
        if (this.props.user.logged_in == true) {

            let user = this.props.user.user;

            if (user.name) this.setState({ name: user.name });
            if (user.username) this.setState({ username: user.username });
            if (user.email) this.setState({ email: user.email });
            if (user.phone) this.setState({ phone: user.phone });
            if (user.photo) this.setState({ thumbnail: user.photo });
        
        }else{
            
            this.props.navigation.navigate('Login');
        }
    }

    async upload(image) {

        try {
            
            let response = await API.post('upload-photo', { image: image });

            if (response) {

                if (response.status == true) {

                    await Local.save('user', response.data);
                    this.setState({ thumbnail: response.data.photo });
                    this.props.user.set(user);
                    
                } else {

                    Local.toast(response.data.toString());

                }

                this.setState({ uploading: false });
            }

        } catch (ex) {

            this.setState({ uploading: false });
        }


    }
    logout = async () => {

        try {

            await Local.logout();
            this.props.user.logout();
            this.props.navigation.navigate('Home');

        } catch (ex) {

            Utils.toast(ex.message.toString());
            this.props.navigation.dispatch(StackActions.reset({ index: 0, key: null, actions: [NavigationActions.navigate({ routeName: 'Home' })] }));
        }

    }

    render() {

        return (

            <Container style={styles.container}>
                <Head title='Profile' />
                <Content>

                    <View style={{ marginTop: 20 }}>
                        <PhotoUpload onPhotoSelect={image => this.upload(image)} onResponse={() => this.setState({ uploading: true })}>
                            {this.state.uploading
                                ? <SkypeIndicator color={Colors.primary} size={80} />
                                : <Thumbnail large source={{ uri: this.state.thumbnail }} style={{ alignSelf: 'center', width : 200, height : 200 }} />
                            }

                        </PhotoUpload>
                    </View>

                    <List style={{ marginTop: 40 }}>

                        <ListItem style={styles.listItem}>
                            <Text style={styles.textRight}>Username</Text>
                            <Text style={styles.textLeft}>{this.state.username}</Text>
                        </ListItem>
                        <ListItem style={styles.listItem}>
                            <Text style={styles.textRight}>Full Name</Text>
                            <Text style={styles.textLeft}>{this.state.name.toUpperCase()}</Text>
                        </ListItem>
                        <ListItem style={styles.listItem}>
                            <Text style={styles.textRight}>Email</Text>
                            <Text style={styles.textLeft}>{this.state.email}</Text>
                        </ListItem>
                        <ListItem style={styles.listItem}>
                            <Text style={styles.textRight}>Phone Number</Text>
                            <Text style={styles.textLeft}>{this.state.phone}</Text>
                        </ListItem>
                        <ListItem style={styles.listItem}>
                            <Text style={styles.textRight}>&nbsp;</Text>
                            <TouchableOpacity onPress={this.logout()}>
                                <Text style={[styles.textLeft, {color : '#aaa'}]}>Logout</Text>
                            </TouchableOpacity>
                        </ListItem>

                    </List>

                </Content>

            </Container>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-end'
    },
    listItem: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        marginBottom: 10
    },
    textRight: {
        flex: 1,
        fontSize: 20,
        color: Colors.grey
    },
    textLeft: {
        flex: 1,
        fontSize: 20,
        textAlign: 'right'
    }

});