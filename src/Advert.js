import React from 'react';
import { TouchableHighlight, Image, Linking } from 'react-native';
import * as Animatable from 'react-native-animatable';
import API from './API';

export default class Advert extends React.Component {

    constructor(props) {

        super(props);
        
        this.state = {

            id : 0,
            uri : null,
            link : null
        }
    }

    async componentDidMount() {

        this.load();
        setInterval(async () => await this.load(), 1000 * 5);
    }

    load = async () => {

        try {
        
            let response = await API.get(`advert/${this.state.id}`);

            console.log(this.state.id);
            
            if (response.status == false) return;
            
            let data = response.data;
            this.setState({ id : data.id, uri: data.image, link: data.link });

        } catch (ex) {

        }
    }


    render() {

        if(this.state.uri == null) return null;

        return (
            
            <TouchableHighlight onPress={() => Linking.openURL(this.state.link)}>
                <Animatable.Image animation='pulse' iterationCount='infinite' direction='alternate' style={{ width: '100%', height: 50 }} source={{ uri: this.state.uri }} />
           </TouchableHighlight>
        )
    }
}