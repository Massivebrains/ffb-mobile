import React from 'react';
import { StyleSheet, FlatList } from 'react-native';
import { Container, Content } from 'native-base';
import { ProgressDialog } from 'react-native-simple-dialogs';
import Head from './Head';
import Chat from './Chat';
const API = require('./API');
const Local = require('./Local');
import { inject, observer } from 'mobx-react';

@inject('user')
@observer
export default class Chats extends React.Component {

    constructor(props) {

        super(props);

        this.state = {
            thumbnail: 'https://res.cloudinary.com/dlk7iozek/image/upload/v1550663534/FFB/1WGjauXxEUtz0wyD26GRUOF8plIrSjrH.png',
            chats: [],
            user: {},
            dialog: false
        }
    }

    async componentDidMount() {

        this.load();
        this.didFocusListener = this.props.navigation.addListener('didFocus', this.load);
    }

    load = async () => {

        try {

            let user = await Local.get('user');

            if (!user || !user.username) {

                await Local.logout();
                this.props.navigation.navigate('Login');

            } else {

                if (this.state.chats.length < 1)
                    this.setState({ dialog: true });

                let response = await API.get(`user/rooms/${user.username}`);

                if (response.status == true) {

                    this.setState({ chats: response.data, user: user, dialog: false });

                    if (response.data.length < 1)
                        this.props.navigation.navigate('Clubs');

                } else {

                    this.setState({ dialog: false });
                    Local.toast(response.data.toString());
                }
            }

        } catch (ex) {

            this.setState({ dialog: false });
            Local.toast(ex.message.toString());
        }
    }

    render() {

        return (

            <Container style={styles.container}>

                <Head title='Chats' navigation={this.props.navigation} />
                <Content>

                    <FlatList noPadding data={this.state.chats} renderItem={({ item }) => <Chat chat={item} user={this.state.user} navigation={this.props.navigation} />} keyExtractor={(item, index) => index.toString()} />

                </Content>

                <ProgressDialog animationType='fade' visible={this.state.dialog} title='Fetching Chats' message='Please wait...' />

            </Container>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-end'
    }

});