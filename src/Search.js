import React from 'react';
import { Header, Item, Icon, Input, Left, Thumbnail } from 'native-base';
const Colors = require('./Colors');
import { inject, observer } from 'mobx-react';

@inject('user')
@observer
export default class Search extends React.Component {

    constructor(props) {

        super(props);

        this.state = {

            thumbnail: 'https://res.cloudinary.com/dlk7iozek/image/upload/v1550663534/FFB/1WGjauXxEUtz0wyD26GRUOF8plIrSjrH.png'
        }
    }

    componentDidMount(){

        if(this.props.user.photo)
            this.setState({thumbnail : this.props.user.photo});
    }

    render() {

        return (

            <Header searchBar style={{ backgroundColor: Colors.primary, height: 50 }} androidStatusBarColor={Colors.primary}>
                <Thumbnail small source={{ uri: this.state.thumbnail }} style={{ marginTop: 10, marginRight: 10 }} />
                <Item rounded style={{ backgroundColor: Colors.primaryLight, borderWidth: 0, borderColor: 'transparent', height: 33 }}>
                    <Icon name="ios-search" style={{ color: Colors.primaryLigthest }} />
                    <Input placeholder="Search" placeholderTextColor='#e0e2ff' style={{ color: Colors.primaryLigthest }} selectionColor={Colors.white} />
                </Item>
                <Icon name='mail' style={{ color: Colors.primaryLigthest, marginTop: 10, marginLeft: 10 }} />
            </Header>
        );
    }
}