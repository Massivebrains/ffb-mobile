import React from 'react';
import { StyleSheet, View, Image, Alert } from 'react-native';
import { Container, Content, Form, Item, Input, Button, Text, H1 } from 'native-base';
import { ProgressDialog } from 'react-native-simple-dialogs';
import Head from './Head';
const API = require('./API');
const Utils = require('./Utils');
const Local = require('./Local');
import { inject, observer } from 'mobx-react';

@inject('user')
@observer
export default class Register extends React.Component {

    constructor(props) {

        super(props);

        this.state = {
            username: null,
            name: null,
            password: null,
            email: null,
            phone: null,
            loading: false,
            dialog: false
        }
    }

    submit = async () => {

        try {

            this.setState({ dialog: true });

            let response = await API.post('register', this.state);

            if (response.status == true) {

                let user = await Local.save('user', response.data);

                if (user) {

                    this.props.user.set(user);
                    this.setState({ dialog: false });
                    this.props.navigation.navigate('Rooms');
                }

            } else {

                this.setState({ dialog: false, password: '' });
                Alert.alert('Registration Failed', response.data.toString());
            }

        } catch (ex) {

            this.setState({ dialog: false, password: '' });
            Local.toast(ex.toString());
        }
    }

    login = () => {

        this.props.navigation.navigate('Login');
    }

    render() {

        return (

            <Container style={styles.container}>

                <Head title='Register' navigation={this.props.navigation} />
                <Content style={{ marginTop: 40 }}>

                    <Image source={require('./images/ffb.png')} style={{ alignSelf: 'center' }} />
                    <H1 style={{ alignSelf: 'center', marginBottom: 20 }}>Football Fans Book</H1>

                    <Form>
                        <Item><Input placeholder='Username' onChangeText={username => this.setState({ username: username })} value={this.state.username} /></Item>
                        <Item><Input placeholder='Full Name' onChangeText={name => this.setState({ name: name })} value={this.state.name} /></Item>
                        <Item><Input placeholder='Email Address' onChangeText={email => this.setState({ email: email })} value={this.state.email} /></Item>
                        <Item><Input placeholder='Phone Number' keyboardType='numeric' onChangeText={phone => this.setState({ phone: phone })} value={this.state.phone} /></Item>
                        <Item><Input placeholder='Password' onChangeText={password => this.setState({ password: password })} value={this.state.password} secureTextEntry={true} /></Item>
                    </Form>

                    <Button onPress={this.submit} rounded block style={{ alignSelf: 'center', elevation: 0, marginLeft: 20, marginTop: 20, marginRight: 20 }}><Text>Register</Text></Button>

                    <View style={{ borderBottomWidth: StyleSheet.hairlineWidth, marginTop: 70 }}></View>

                    <Button onPress={this.login} transparent block style={{ alignSelf: 'center', elevation: 0, marginLeft: 20, marginRight: 20 }}><Text>or Login</Text></Button>

                </Content>

                <ProgressDialog animationType='fade' visible={this.state.dialog} title='Loading' message='Please wait...' />

            </Container>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-end'
    }

});