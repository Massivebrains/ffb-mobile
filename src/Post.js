import React from 'react';
import { StyleSheet, View, TouchableHighlight } from 'react-native';
import { Text, Thumbnail, Badge } from 'native-base';
import Moment from 'moment';
const Colors = require('./Colors');
import Local from './Local';

export default class Post extends React.Component {

    constructor(props) {

        super(props);

        this.state = {

            post: {},
            id: null,
            title: null,
            content: null,
            date: null,
            excerpt: 'Loading...',
            featured_image: null,
            tags: null
        }

    }

    componentDidMount() {

        this.setState({ featured_image: Local.defaultLogo });

        if (this.props.post) {

            let post = this.props.post;

            this.setState({ post: post });
            if (post.id) this.setState({ id: post.reference });
            if (post.title) this.setState({ title: post.title });
            if (post.content) this.setState({ content: post.content });
            if (post.excerpt) this.setState({ excerpt: post.excerpt });
            if (post.publish_date) this.setState({ date: post.publish_date });
            if (post.featured_image && post.featured_image.length > 5) this.setState({ featured_image: post.featured_image });

            if (post.tags.length > 0) {

                let tags = '';

                post.tags.forEach(row => {

                    tags = `${tags}+ ${row.name} `;
                });

                tags = tags.substring(0, tags.length - 1);

                this.setState({ tags: tags });
            }
        }
    }

    openPostDetail = () => {

        this.props.navigation.navigate('PostDetail', { post: this.state.post });
    }

    render() {

        return (

            <View style={styles.tweet}>

                <TouchableHighlight underlayColor='white' activeOpacity={0.75} onPress={this.openPostDetail}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <Thumbnail source={{ uri: this.state.featured_image }} />
                        <View style={{ flexDirection: 'column', justifyContent: 'flex-start' }}>
                            <Text style={{ paddingLeft: 15, fontWeight: 'bold', color: Colors.black, fontSize: 17, flexWrap: 'wrap', width: 320 }}>{this.state.title}</Text>
                            <Text style={{ paddingLeft: 15, color: '#aaa', fontSize: 12 }}>{Moment(this.state.data).format('MMMM Do YYYY, h:mm a')}</Text>
                        </View>
                    </View>
                </TouchableHighlight>

                <TouchableHighlight underlayColor='white' activeOpacity={0.75} onPress={this.openPostDetail}>
                    <Text style={styles.tweetText}>{this.state.excerpt}</Text>
                </TouchableHighlight>

                <TouchableHighlight underlayColor='white' activeOpacity={0.75} onPress={this.openPostDetail}>
                    <View style={styles.tweetFooter}>
                        <View style={styles.footerIcons}>

                            {this.state.tags
                                ? <Badge style={{ backgroundColor: 'transparent' }}>
                                    <Text style={{ fontSize: 12, color: Colors.grey }}>{this.state.tags}</Text>
                                </Badge>
                                : null}
                        </View>
                    </View>
                </TouchableHighlight>
            </View>
        );
    }
}

const styles = StyleSheet.create({

    topMargin: {
        backgroundColor: "white",
        zIndex: -1
    },
    content: {
        padding: 10,
        backgroundColor: "white"
    },
    heading: {
        fontSize: 32,
        fontWeight: "400",
        marginBottom: 30
    },
    tweet: {
        paddingTop: 20,
        paddingBottom: 5,
        paddingLeft: 10,
        paddingRight: 10,
        borderBottomColor: "#bbb",
        borderBottomWidth: StyleSheet.hairlineWidth,
        flexDirection: "column"
    },
    tweetText: {
        marginTop: 10,
        fontSize: 16,
        color: "#555"
    },
    tweetFooter: {
        flexDirection: 'row',
        justifyContent: 'flex-end',
        padding: 0
    },
    badgeCount: {
        fontSize: 12,
        paddingLeft: 1
    },
    footerIcons: {
        flexDirection: "row",
        alignItems: "center"
    }
});