import React from 'react';
import { StyleSheet, Image } from 'react-native';
import { Container, Content, Card, CardItem, Body, H3 } from 'native-base';
import { Html } from '@shoutem/ui';
import Head from './Head';
import Advert from './Advert';


export default class Home extends React.Component {

    static navigationOptions = { header: null }

    constructor(props) {

        super(props);

        this.state = {

            id: '',
            date: '',
            slug: '',
            status: '',
            title: '',
            content: 'Loading...',
            excerpt: '',
            body: null,
            image: null,
            link: ''
        }

    }

    componentDidMount() {

        let post = this.props.navigation.getParam('post');

        if (post) {

            if (post.id) this.setState({ id: post.reference });
            if (post.date) this.setState({ date: post.date });
            if (post.slug) this.setState({ slug: post.slug });
            if (post.status) this.setState({ date: post.status });
            if (post.title) this.setState({ title: post.title });
            if (post.body) this.setState({ body: post.body });
            if (post.excerpt) this.setState({ excerpt: post.excerpt });
            if (post.featured_image && post.featured_image.length > 10) this.setState({ image: post.featured_image });
            if (post.link) this.setState({ link: post.link });
        }
    }


    render() {

        return (

            <Container style={styles.container}>

                <Head title='Post' back='Tabs' navigation={this.props.navigation} />

                <Content>

                    <Advert />

                    <Card transparent>
                        <CardItem>
                            <Body>

                                {this.state.image
                                    ? <Image source={this.state.image}/>
                                    : null}

                                <H3 style={{ marginBottom: 5 }}>{this.state.title}</H3>

                                {this.state.body
                                    ? <Html body={this.state.body} />
                                    : null}
                            </Body>
                        </CardItem>
                    </Card>

                </Content>

            </Container >
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-end',
        fontFamily: 'monospace'
    }
});