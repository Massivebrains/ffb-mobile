import React from 'react';
import { Text, ListItem, Left, Body, Right, Thumbnail } from 'native-base';

export default class Chat extends React.Component {

    constructor(props) {

        super(props);

        this.state = {
            room_id: 0,
            name: 'LiverPool',
            description: 'The FFB Chat Book',
            username: '',
            image: 'https://res.cloudinary.com/dlk7iozek/image/upload/v1550663534/FFB/1WGjauXxEUtz0wyD26GRUOF8plIrSjrH.png'
        }
    }

    async componentDidMount() {

        this.load();
    }

    joinChat = async () => {

        this.props.navigation.navigate('ChatSession', { chat: this.state });
    }

    load = async () => {

        try {

            let chat = this.props.chat;
            let user = this.props.user;

            this.setState({ room_id: chat.pusher_id, image: chat.image, name: chat.name, description: chat.description, username: user.username });

        } catch (ex) {


        }
    }

    render() {

        return (

            <ListItem thumbnail onPress={this.joinChat}>

                <Left>
                    <Thumbnail square source={{ uri: this.state.image }} />
                </Left>

                <Body>
                    <Text>{this.state.name}</Text>
                    <Text note numberOfLines={1}>{this.state.description}</Text>
                </Body>

                <Right>

                </Right>

            </ListItem>
        );

    }
}