import React from 'react';
import { StyleSheet, Alert, View, Image } from 'react-native';
import { Container, Content, Form, Item, Input, Button, Text, H1 } from 'native-base';
import { ProgressDialog } from 'react-native-simple-dialogs';
import Head from './Head';
const API = require('./API');
const Utils = require('./Utils');
const Local = require('./Local');
import { inject, observer } from 'mobx-react';

@inject('user')
@observer
export default class Login extends React.Component {

    constructor(props) {

        super(props);
        this.state = { username: '', password: '', loading: false, dialog: false }
    }

    submit = async () => {

        try {

            this.setState({ dialog: true });

            let response = await API.post('login', { username: this.state.username, password: this.state.password });

            if (response.status == true) {

                let user = await Local.save('user', response.data);

                if (user) {

                    this.props.user.set(user);
                    this.setState({ dialog: false });
                    this.props.navigation.navigate('Chats');
                }

            } else {

                this.setState({ dialog: false, password: '' });
                Alert.alert('Error', response.data.toString());
            }

        } catch (ex) {

            this.setState({ dialog: false, password: '' });
            Alert.alert('Error', ex.toString());
        }
    }

    register = () => {

        this.props.navigation.navigate('Register');
    }

    render() {

        return (

            <Container style={styles.container}>

                <Head title='Login' navigation={this.props.navigation} />
                <Content style={{ marginTop: 40 }}>

                    <Image source={require('./images/ffb.png')} style={{ alignSelf: 'center' }} />
                    <H1 style={{ alignSelf: 'center', marginBottom: 20 }}>Football Fans Book</H1>

                    <Form>
                        <Item><Input placeholder='Username' onChangeText={username => this.setState({ username: username })} value={this.state.username} /></Item>
                        <Item><Input placeholder='Password' onChangeText={password => this.setState({ password: password })} value={this.state.password} secureTextEntry={true} /></Item>
                    </Form>

                    <Button onPress={this.submit} rounded block style={{ alignSelf: 'center', elevation: 0, marginLeft: 20, marginTop: 20, marginRight: 20 }}><Text>Login</Text></Button>

                    <View style={{ borderBottomWidth: StyleSheet.hairlineWidth, marginTop: 70 }}></View>

                    <Button onPress={this.register} transparent block style={{ alignSelf: 'center', elevation: 0, marginLeft: 20, marginRight: 20 }}><Text>or Register Now</Text></Button>

                </Content>

                <ProgressDialog animationType='fade' visible={this.state.dialog} title='Loading' message='Please wait...' />

            </Container>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-end'
    }

});