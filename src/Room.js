import React from 'react';
import { Text, ListItem, Left, Body, Right, Icon, Thumbnail } from 'native-base';
import { ConfirmDialog } from 'react-native-simple-dialogs';
import Chat from './Chat';
const API = require('./API');
const Utils = require('./Utils');
const Local = require('./Local');
const Colors = require('./Colors');

export default class Room extends React.Component {

    constructor(props) {

        super(props);

        this.state = {
            id: 0,
            pusher_id: 0,
            name: '',
            users_count: 0,
            description: '',
            image: 'https://upload.wikimedia.org/wikipedia/hif/8/82/Arsenal_FC.png',
            dialog: false
        }
    }

    async componentDidMount() {

        this.load();
    }

    joinRoom = () => this.setState({ dialog: true });

    yes = async () => {

        try {

            this.setState({ dialog: false });

            let user = await Local.get('user');

            if (!user || !user.username) {

                await Local.logout();
                Utils.toast('Please login or create an account to join a chat room.');
                this.props.navigation.navigate('Login');

            } else {

                let response = await API.get(`room/join/${this.state.pusher_id}/${user.username}`);
                Utils.toast(response.data);

                if (response.status == false) {

                    this.props.navigation.navigate('Rooms');

                } else {

                    this.props.navigation.navigate('Chats', { room: this.state.pusher_id });
                }
            }

        } catch (ex) {

            this.setState({ dialog: false });
            Utils.toast();
        }
    }

    no = () => {

        this.setState({ dialog: false });
    }

    load = async () => {

        try {

            let room = this.props.room;

            if (room.id) this.setState({ id: room.id });
            if (room.pusher_id) this.setState({ pusher_id: room.pusher_id });
            if (room.name) this.setState({ name: room.name });
            if (room.description) this.setState({ description: room.description });
            if (room.image) this.setState({ image: room.image });
            if (room.users_count) this.setState({ users_count: room.users_count });

        } catch (ex) {


        }
    }

    render() {

        return (

            <ListItem thumbnail onPress={this.joinRoom}>
                <Left>
                    <Thumbnail square source={{ uri: this.state.image }} />
                </Left>
                <Body>
                    <Text>{this.state.name}</Text>
                    <Text note numberOfLines={1}>{this.state.description}</Text>
                </Body>

                <Right>
                    <Text note numberOfLines={1}>{this.state.users_count} {this.state.users_count > 1 ? 'Members' : 'Member'}</Text>
                </Right>

                <ConfirmDialog title={this.state.name} message='By Tapping Yes, you agree to all of our Terms and Conditions.' visible={this.state.dialog} onTouchOutside={() => this.setState({ dialog: false })} positiveButton={{ title: 'YES', onPress: () => this.yes() }} negativeButton={{ title: 'No', onPress: () => this.no() }} />

            </ListItem>
        );

    }
}