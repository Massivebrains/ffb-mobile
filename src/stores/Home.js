import { observable, action } from 'mobx';

export default class Home {

    @observable data = [];
    @observable loaded = false;

    @action set = async (data) => {

        this.data = data;
        this.loaded = true;

    }
}