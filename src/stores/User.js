import { observable, action } from 'mobx';

export default class User {

    @observable user = {};
    @observable api_token = null;
    @observable name = null;
    @observable username = null;
    @observable email = null;
    @observable phone = null;
    @observable photo = null;
    @observable logged_in = false;
    @observable last_login = false;

    @action set = async (user) => {

        if (user) {

            this.user = user;
            this.name = user.name;
            this.username = user.username;
            this.email = user.email;
            this.phone = user.phone;
            this.photo = user.photo;
            this.api_token = user.api_token;
            this.logged_in = true;
            this.last_login = user.last_login;
        }

    }

    @action logout = async () => {

        if (user) {

            this.user = {};
            this.name = null;
            this.username = null;
            this.phone = null;
            this.photo = null;
            this.api_token = null;
            this.logged_in = false;
            this.last_login = null;
        }

    }
}