import React from 'react';
import { StyleSheet, FlatList, Image } from 'react-native';
import { Container, Content, Tab, Tabs, ScrollableTab } from 'native-base';
const Colors = require('./Colors');
import Search from './Search';
const Utils = require('./Utils');
const API = require('./API');
import Post from './Post';
import Advert from './Advert';
import { inject, observer } from 'mobx-react';
import Loading from './Loading';
const Local = require('./Local');

@inject('user')
@inject('home')
@observer
export default class Home extends React.Component {

    constructor(props) {

        super(props);

        this.state = { for_you: [], entertainment: [], football: [] }
    }

    async componentDidMount() {

        try {

            if (this.props.home.loaded == false) {

                let user = await Local.get('user');
                if (user) this.props.user.set(user);

                let data = this.props.home.data;
                this.setState({ for_you: data, entertainment: data, football: data });
            }

            let response = await API.get('posts');

            if (response.status == false) {

                Utils.toast();

            } else {

                let data = response.data;
                this.props.home.set(data);
                this.setState({ for_you: data, entertainment: data, football: data });
            }

        } catch (ex) {

            Utils.toast();
            this.props.home.set([]);
        }
    }
    
    render() {

        if (this.props.home.loaded == false) return (<Loading title='Home' navigation={this.props.navigation} search={true} />);

        return (

            <Container style={styles.container}>

                <Search />

                <Content>

                    <Tabs renderTabBar={() => <ScrollableTab style={{ borderBottomWidth: 0, padding: 0 }} />} tabBarUnderlineStyle={{ borderBottomWidth: 0 }} tabBarBackgroundColor={Colors.white}>

                        <Tab heading="For You" tabStyle={styles.tabStyle} textStyle={styles.textStyle} activeTabStyle={styles.activeTabStyle} activeTextStyle={styles.activeTextStyle}>



                            <FlatList noPadding data={this.state.for_you} renderItem={({ item }) => <Post post={item} navigation={this.props.navigation} />} keyExtractor={(item, index) => index.toString()} />

                        </Tab>

                        <Tab heading="Entertainment" tabStyle={styles.tabStyle} textStyle={styles.textStyle} activeTabStyle={styles.activeTabStyle} activeTextStyle={styles.activeTextStyle}>

                            <FlatList noPadding data={this.state.entertainment} renderItem={({ item }) => <Post post={item} navigation={this.props.navigation} />} keyExtractor={(item, index) => index.toString()} />

                        </Tab>

                        <Tab heading="Football" tabStyle={styles.tabStyle} textStyle={styles.textStyle} activeTabStyle={styles.activeTabStyle} activeTextStyle={styles.activeTextStyle}>

                            <FlatList noPadding data={this.state.football} renderItem={({ item }) => <Post post={item} navigation={this.props.navigation} />} keyExtractor={(item, index) => index.toString()} />

                        </Tab>

                    </Tabs>

                </Content>

            </Container >
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-end',
        fontFamily: 'monospace'
    },
    item: {
        flex: 1,
        width: '100%'
    },
    input: {
        flex: 1,
        width: '100%'
    },
    tabStyle: {

        backgroundColor: 'transparent',
        borderBottomWidth: 0,
        borderBottomColor: 'transparent',
        height: 30
    },
    textStyle: {
        color: 'grey',
    },
    activeTabStyle: {
        backgroundColor: 'transparent',
        height: 30,
        borderBottomWidth: 1,
        borderBottomColor: Colors.primary
    },
    activeTextStyle: {
        color: Colors.primary
    }
});