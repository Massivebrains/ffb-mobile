import React from 'react';
import { StyleSheet, Alert, FlatList } from 'react-native';
import { Container, Content, Separator, Text } from 'native-base';
import { ProgressDialog } from 'react-native-simple-dialogs';
import Head from './Head';
import Room from './Room';
const API = require('./API');
const Local = require('./Local');
import { inject, observer } from 'mobx-react';

@inject('user')
@observer
export default class Rooms extends React.Component {

    constructor(props) {

        super(props);

        this.state = {
            thumbnail: 'https://i.pinimg.com/originals/7d/1a/3f/7d1a3f77eee9f34782c6f88e97a6c888.jpg',
            rooms: [],
            dialog: false
        }
    }

    async componentDidMount() {

        this.load();
        this.didFocusListener = this.props.navigation.addListener('didFocus', this.load);
    }

    load = async () => {

        try {

            let user = await Local.get('user');

            if (!user || !user.username) {

                await Local.logout();
                this.props.navigation.navigate('Login');

            } else {

                if (this.state.rooms.length < 1)
                    this.setState({ dialog: true });

                let response = await API.get('rooms');

                if (response.status == true) {

                    this.setState({ rooms: response.data, dialog: false });

                } else {

                    this.setState({ dialog: false });
                    Alert.alert('Error', response.data.toString());
                }
            }



        } catch (ex) {

            this.setState({ dialog: false, password: '' });
            Alert.alert('Error', ex.toString());
        }
    }

    render() {

        return (

            <Container style={styles.container}>

                <Head title='Clubs' navigation={this.props.navigation} />
                <Content>

                    <Separator bordered>
                        <Text style={{ fontSize: 15 }}>Select your football club. Join your co-fans!</Text>
                    </Separator>

                    <FlatList noPadding data={this.state.rooms} renderItem={({ item }) => <Room room={item} navigation={this.props.navigation} />} keyExtractor={(item, index) => index.toString()} />

                </Content>

                <ProgressDialog animationType='fade' visible={this.state.dialog} title='Fetching FFB Clubs' message='Please wait...' />

            </Container>
        );
    }
}

const styles = StyleSheet.create({

    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-end'
    }

});