import React from 'react';
import { View, StatusBar } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { createBottomTabNavigator, createStackNavigator } from 'react-navigation';
import { Provider } from 'mobx-react';
const Colors = require('./src/Colors');
import stores from './stores';
import Home from './src/Home';
import PostDetail from './src/PostDetail';
import Profile from './src/Profile';
import Auth from './src/Auth';
import Register from './src/Register';
import Login from './src/Login';
import Rooms from './src/Rooms';
import Chats from './src/Chats';
import ChatSession from './src/ChatSession';

export default class App extends React.Component {

  render() {

    return (

      <Provider {...stores}>
        <View style={{ flex: 1, justifyContent: 'center' }}>
          <StatusBar backgroundColor={Colors.primary} />
          <Stack />
        </View>
      </Provider>
    );
  }
}

const Tabs = createBottomTabNavigator({

  Home: { screen: Home },
  Chats: { screen: Chats },
  // Clubs: { screen: Rooms },
  Profile: { screen: Profile }

}, {
    navigationOptions: ({ navigation }) => ({

      tabBarIcon: ({ focused, horizontal, tintColor }) => {

        const { routeName } = navigation.state;
        let icon;

        switch (routeName) {

          case 'Home': icon = 'md-home'; break;
          case 'Chats': icon = 'md-mail'; break;
          case 'Clubs': icon = 'md-people'; break;
          case 'Profile': icon = 'md-contact'; break;
          default: icon = 'md-home'; break;

        }

        return <Ionicons name={icon} size={horizontal ? 20 : 25} color={tintColor} />;
      },
    }),
    tabBarOptions: { activeTintColor: Colors.orange, inactiveTintColor: '#b3b8fc', tabStyle: { paddingBottom: 5, paddingTop: 10, fontSize: 5, backgroundColor: Colors.primary } }, initialRouteName: 'Home'
  })

const Stack = createStackNavigator({

  Tabs: { screen: Tabs },
  Auth: { screen: Auth, navigationOptions: { header: null } },
  Register: { screen: Register, navigationOptions: { header: null } },
  Login: { screen: Login, navigationOptions: { header: null } },
  PostDetail: { screen: PostDetail, navigationOptions: { header: null } },
  Rooms: { screen: Rooms, navigationOptions: { header: null } },
  ChatSession: { screen: ChatSession, navigationOptions: { header: null } },

}, { navigationOptions: { header: null } });